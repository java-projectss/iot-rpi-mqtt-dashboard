package com.mycompany;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.pi4j.io.exception.IOException;
import com.mycompany.ProcessBuilder.ProcessBuilderDht;
import com.mycompany.ProcessBuilder.ProcessBuilderDoorbell;
import com.mycompany.ProcessBuilder.ProcessBuilderSensor;

/**
 *
 * @author Carlton Davis
 */

/* Test class for ProcessBuilderEx class */
public class ProcessBuilderExTest {

    @Test
    public void DHTProcessBuilderTesting() throws IOException, java.io.IOException {
        String theCmd = "src/main/Python/DHT11.py";
        ProcessBuilderDht processBuilderTemp = new ProcessBuilderDht(theCmd);
        String temp = processBuilderTemp.startProcessDHT("Temperature");
        assertNotNull(temp);
    }

    @Test
    public void ProcessTesting() throws IOException, java.io.IOException {
        String theCmdDoorbell = "src/main/Python/Doorbell.py";
        ProcessBuilderDoorbell processBuilderTemp = new ProcessBuilderDoorbell(theCmdDoorbell);
        String doorBell = processBuilderTemp.startProcessDoorbell();
        assertNotNull(doorBell);
    }

    @Test
    public void ProcessDoorBellTesting() throws IOException, java.io.IOException {
        String theCmdDoorbell = "src/main/Python/Doorbell.py";
        ProcessBuilderDoorbell processBuilderTemp = new ProcessBuilderDoorbell(theCmdDoorbell);
        String doorBell = processBuilderTemp.startProcessDoorbell();
        assertNotNull(doorBell);
    }

    @Test
    public void ProcessMotionTesting() throws IOException, java.io.IOException {
        String theCmdMotionSensor = "src/main/Python/MotionSensor.py";
        ProcessBuilderSensor processBuilderTemp = new ProcessBuilderSensor(theCmdMotionSensor);
        String motionSensor = processBuilderTemp.startProcessSensor();
        assertNotNull(motionSensor);
    }
}
