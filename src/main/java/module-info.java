module DataComProject {

    requires com.pi4j;
    requires com.pi4j.plugin.pigpio;
    
    requires org.slf4j;
    requires org.slf4j.simple;

    requires java.logging;

    requires javafx.graphics;
    requires com.hivemq.client.mqtt;
    requires eu.hansolo.tilesfx;
    requires org.json;
    requires java.desktop;

    uses com.pi4j.extension.Extension;
    uses com.pi4j.provider.Provider;
}