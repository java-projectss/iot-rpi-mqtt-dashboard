package com.mycompany;

import com.mycompany.crypto.HashingCode;
import java.io.IOException;

import com.hivemq.client.mqtt.exceptions.ConnectionFailedException;
import com.hivemq.client.mqtt.mqtt5.exceptions.Mqtt5ConnAckException;
import com.mycompany.ProcessBuilder.ProcessBuilderDht;
import com.mycompany.ProcessBuilder.ProcessBuilderDoorbell;
import com.mycompany.ProcessBuilder.ProcessBuilderSensor;
import com.mycompany.keyStore.KStore;
import com.mycompany.publisherTiles.PublisherTilesController;
import com.mycompany.utils.ImageProcessor;
import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.Tile.TextSize;
import eu.hansolo.tilesfx.Tile.SkinType;
import eu.hansolo.tilesfx.TileBuilder;
import eu.hansolo.tilesfx.tools.FlowGridPane;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import org.json.JSONObject;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Locale;

import static com.hivemq.client.mqtt.MqttGlobalPublishFilter.SUBSCRIBED;
import com.hivemq.client.mqtt.datatypes.MqttQos;
import com.mycompany.mqtt.MqttTopicsOfUsersRPI;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;
import java.time.format.DateTimeParseException;
import java.util.logging.Level;

/**
 * @author Cuneyt Yildirim, Harout Dabbaghian, Davit Voskerchyan
 */

/* Class to Create the GUI with the help of TilesFX library */
public class FXScreen extends FlowGridPane {

    //Stores timestamp generated within the  exampleThread method
    private final Tile pOneTempTile, pOneHumidTile, pOneMotionTile, pOneBuzzerTile, pTwoTempTile,
            pTwoHumidTile, pTwoMotionTile, pTwoBuzzerTile, myTempTile, myHumidTile, myMotionTile,
            myBuzzerTile, myCockTile, exitTile, pOneImageTile, myImageTile, pTwoImageTile;
    public static Tile myErrorDisplayTile;
    public static User USER;
    private VBox userInput, mqttBox;
    private ProcessBuilderDht theProcessBuilderDht;
    private ProcessBuilderDoorbell theProcessBuilderDoorbell;
    private ProcessBuilderSensor theProcessBuilderSesnsor;

    private static boolean running = true;
    private final int WIDTH = 400;
    private final int HEIGHT = 200;

    private boolean isUserLoggedIn = false;

    Logger logger = Logger.getLogger(this.getClass().getName());

    //Constructor
    public FXScreen() {
        super(5, 4);

        //Setup event handler for the exit button
        Button exitButton = new Button("Exit");
        exitButton.setOnAction(e -> endApplication());
        setHgap(10);
        setVgap(10);
        setAlignment(Pos.CENTER);
        setPadding(new Insets(20));
        setBackground(new Background(new BackgroundFill(Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        // Defining Login Tiles
        userInput = new VBox();
        userInput.setLayoutX(40);
        userInput.setMaxWidth(WIDTH);
        userInput.setMaxHeight(HEIGHT);
        DropShadow shadow = new DropShadow();
        TextField userIDInput = new TextField();
        userIDInput.setPromptText("UserName");
        userIDInput.setAlignment(Pos.CENTER);//Align text to center
        userIDInput.setMaxWidth(WIDTH - 100);
        userIDInput.setPrefHeight(40);
        // Password
        PasswordField userPassword = new PasswordField();
        userPassword.setAlignment(Pos.CENTER);//Align text to center
        userPassword.setMaxWidth(WIDTH - 100);
        userPassword.setPrefHeight(40);
        userPassword.setPromptText("Enter Password");
        VBox.setMargin(userPassword, new Insets(15, 0, 15, 0));
        // Submit login button
        Button submitButton = new Button("Submit");
        submitButton.setAlignment(Pos.CENTER);
        submitButton.setMaxWidth(WIDTH - 150);
        submitButton.setPrefHeight(40);
        submitButton.setStyle("-fx-background-color: #05f611; -fx-text-fill: #035e0f");
        Font font = Font.font("Cascadia", FontWeight.BOLD, 20);

        submitButton.setFont(font);
        // Add children for login
        userInput.getChildren().addAll(userIDInput, userPassword, submitButton);

        // Defining Tiles
        myErrorDisplayTile = createTile(SkinType.TEXT, "Notify Results", "");
        myErrorDisplayTile.setTitleColor(Color.RED);
        myErrorDisplayTile.setTextColor(Color.RED);
        myErrorDisplayTile.setDescriptionAlignment(Pos.CENTER);

        // MY Clock Tile
        myCockTile = TileBuilder.create()
                .skinType(SkinType.CLOCK)
                .prefSize(WIDTH, HEIGHT)
                .title("Clock Display")
                .dateVisible(true)
                .textSize(TextSize.SMALL)
                .locale(Locale.CANADA)
                .running(true)
                .dateColor(Color.BLUEVIOLET)
                .titleColor(Color.BLUEVIOLET)
                .backgroundColor(Color.valueOf("#393d42"))
                .build();
        // My Exit Tile
        exitTile = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(WIDTH, HEIGHT)
                .textSize(TextSize.BIGGER)
                .title("Quit the application")
                .graphic(exitButton)
                .roundedCorners(false)
                .backgroundColor(Color.valueOf("#393d42"))
                .build();

        // Publisher 1 Tiles
        pOneTempTile = createTile(SkinType.GAUGE, "Publsiher one Temperature", "C");
        pOneHumidTile = createTile(SkinType.PERCENTAGE, "Publsiher one Humid", "%");
        pOneMotionTile = createTile(SkinType.TEXT, "Publsiher one Motion", "cm");
        pOneBuzzerTile = createTile(SkinType.TEXT, "Publsiher one Buzzer", "Hz");
        pOneImageTile = ImageTileGenerator("/images/sunny-clip-art.png", "Publsiher one Camera Image");
        // Publisher 2 Tiles
        pTwoTempTile = createTile(SkinType.GAUGE, "Publisher two Temperature", "C");
        pTwoHumidTile = createTile(SkinType.PERCENTAGE, "Publisher two Humid", "%");
        pTwoMotionTile = createTile(SkinType.TEXT, "Publisher two Motion", "cm");
        pTwoBuzzerTile = createTile(SkinType.TEXT, "Publisher two Buzzer", "Hz");
        pTwoImageTile = ImageTileGenerator("/images/sunny-clip-art.png", "Publisher two Camera Image");
        // My Tiles
        myTempTile = createTile(SkinType.GAUGE, "My Temperature", "C");
        myHumidTile = createTile(SkinType.PERCENTAGE, "My Humid", "%");
        myMotionTile = createTile(SkinType.TEXT, "My Motion", "cm");
        myBuzzerTile = createTile(SkinType.TEXT, "My Buzzer", "Hz");
        myImageTile = ImageTileGenerator("/images/sunny-clip-art.png", "My Camera Image");

        // Set Up Publish Mqtt Button & VBox
        mqttBox = new VBox();
        mqttBox.setLayoutX(100);
        mqttBox.setMaxWidth(WIDTH);
        mqttBox.setMaxHeight(HEIGHT);
        mqttBox.setAlignment(Pos.CENTER);
        mqttBox.setPrefHeight(40);
        // Button
        Button publishPublicKeyBTN = new Button("Publish Public Key");
        publishPublicKeyBTN.setAlignment(Pos.CENTER);
        publishPublicKeyBTN.setMaxWidth(WIDTH - 100);
        publishPublicKeyBTN.setPrefHeight(40);
        publishPublicKeyBTN.setStyle("-fx-background-color: #05f611; -fx-text-fill: #035e0f");
        publishPublicKeyBTN.setDisable(true);
        // Add Children
        mqttBox.getChildren().addAll(publishPublicKeyBTN);

        //Dht process builder
        String theCmd = "src/main/Python/DHT11.py";
        theProcessBuilderDht = new ProcessBuilderDht(theCmd);

        //Doorbell process builder
        String theCmdDoorbell = "src/main/Python/Doorbell.py";
        theProcessBuilderDoorbell = new ProcessBuilderDoorbell(theCmdDoorbell);

        //Motion process builder
        String theCmdMotionSensor = "src/main/Python/MotionSensor.py";
        theProcessBuilderSesnsor = new ProcessBuilderSensor(theCmdMotionSensor);
         
        // Add to scene/Stage
        getChildren().addAll(userInput,
                myCockTile,
                exitTile,
                myErrorDisplayTile,
                mqttBox,
                pOneTempTile,
                pOneHumidTile,
                pOneMotionTile,
                pOneBuzzerTile,
                pOneImageTile,
                pTwoTempTile,
                pTwoHumidTile,
                pTwoMotionTile,
                pTwoBuzzerTile,
                pTwoImageTile,
                myTempTile,
                myHumidTile,
                myMotionTile,
                myBuzzerTile,
                myImageTile
        );

        // Effects for Submit Button
        submitButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> submitButton.setEffect(shadow));
        submitButton.addEventHandler(MouseEvent.MOUSE_EXITED, e -> submitButton.setEffect(null));
        
        // Effects for Submit Button
        publishPublicKeyBTN.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> publishPublicKeyBTN.setEffect(shadow));
        publishPublicKeyBTN.addEventHandler(MouseEvent.MOUSE_EXITED, e -> publishPublicKeyBTN.setEffect(null));
        
        // Add Publish key listener
        publishPublicKeyBTN.setOnAction(e -> publishPublicKey());

        // Error font for submit button
        Font errorFontSubmitBtn = Font.font("Arial", FontWeight.SEMI_BOLD, 10);
        
        // Submit Button to create User & MQTT connection
        submitButton.setOnAction(e -> {
            submitButton.setFont(errorFontSubmitBtn);
            // Check if user put his username
            if (userIDInput.getText().isEmpty() || userIDInput.getText().length() < 5) {
                userIDInput.setStyle("-fx-border-color:#FF0000;-fx-text-fill: #BF0909; -fx-font-size: 10");
                // Notify User of Error
                userIDInput.setPromptText("Please enter username of at least 5 characters.");
                setButtonErrorStyle(submitButton, "Username must be at least 5 characters.Try Again");
                // We need userID hence skip the rest of the function
                return;
            } else {
                userIDInput.setPromptText("");
                userIDInput.setStyle(null);
            }

            // Check if user put his password
            if (userPassword.getText().isEmpty() || userPassword.getText().length() < 8) {
                userPassword.setStyle("-fx-border-color:#FF0000");
                // Notify User of Error
                userPassword.setPromptText("Enter a password of at least 8 characters.");
                setButtonErrorStyle(submitButton, "Password must be at least 8 characters. Try Again");
                // We need password hence skip the rest of the function
                return;
            } else {
                userPassword.setStyle(null);
            }

            // Thread for connection to MQTT
            Thread mqttThread = new Thread(() -> {
                try {
                    // Create User with Mqtt connection
                    FXScreen.USER = new User(userIDInput.getText(), userPassword.getText());
                    System.out.println("CREATED USER.");
                    publishPublicKey();
                    System.out.println("User Published Public Key.");
                    Platform.runLater(() -> {
                        // Reset Submit Button
                        submitButton.setFont(font);
                        submitButton.setText("User Created.");
                        submitButton.setDisable(true);
                        submitButton.setStyle("-fx-background-color: #05f611; -fx-text-fill: #035e0f");
                    });
                    // Subscribe to Topics
                    MyTopicSubscriptions.subscribeToMyTopics(FXScreen.USER.getMyMqttConnection());
                    // Watch for new published messages
                    observeMySubscribedTopicsPublishesRPI();
                    publishPublicKeyBTN.setDisable(false);
                    // Start Process BUilders
                    startDisplaySensorsData();
                    startDisplayBuzzerData();
                    startDisplayMotionData();
                    isUserLoggedIn = true;
                } catch (InvalidAlgorithmParameterException | NoSuchPaddingException | IllegalBlockSizeException
                        | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException ex) {
                    Platform.runLater(() -> setButtonErrorStyle(submitButton, "Contact Admin. Problem with User creation."));
                } catch (Mqtt5ConnAckException ex) {
                    Platform.runLater(() -> setButtonErrorStyle(submitButton, "Invalid UserName or Password. Try Again"));
                } catch (ConnectionFailedException ex) {
                    Platform.runLater(() -> setButtonErrorStyle(submitButton, "Connection failed. Check host url and port."));
                } catch (IllegalArgumentException ex) {
                    Platform.runLater(() -> setButtonErrorStyle(submitButton, "Port should be between 0-65535. Check host port."));
                } catch (IOException ex) {
                    Platform.runLater(() -> setButtonErrorStyle(submitButton, "KeyStore password does not match HiveMq."));
                }
            });
            submitButton.setText("Loading User ");
            mqttThread.start();
        });
    }
    
    /**
     * This method Published Users public key to HiveMQ
     */
    private void publishPublicKey() {
        String encodedPublicKey = HashingCode.base64EncodeString(FXScreen.USER.getKeyStore().getPublicKey().getEncoded());
        String publicKeyMessage = "{\"publickey\":\"" + encodedPublicKey + "\",\"alias\":\"" + MyTopicSubscriptions.whoAmI + "-pk\"}";

        FXScreen.USER.getMyMqttConnection().publishWith()
                .topic(MyTopicSubscriptions.whoAmI + "/publicKey")
                .payload(publicKeyMessage.getBytes(UTF_8))
                .retain(true)
                .qos(MqttQos.EXACTLY_ONCE)
                .send();
        logger.info("Public Key Was published - publishPublicKey()");
    }

    /**
     * This method observes the message published to the topics that the user is
     * subscribed too.
     */
    private void observeMySubscribedTopicsPublishesRPI() {
        FXScreen.USER.getMyMqttConnection().toAsync().publishes(SUBSCRIBED, publish -> {
            try {
                logger.info(publish.getTopic().toString());
                // Save Public Key
                if (publish.getTopic().toString().contains("publicKey")) {

                    JSONObject payload = new JSONObject(UTF_8.decode(publish.getPayload().get()).toString());
                    // Recreate a PublicKey Object
                    byte[] publicKeyByte = Base64.getDecoder().decode(payload.getString("publickey"));
                    String alias = payload.getString("alias");
                    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyByte);
                    KeyFactory keyFactory = KeyFactory.getInstance(FXScreen.USER.getKeyStore().getPublicKey().getAlgorithm());
                    PublicKey pubKey = keyFactory.generatePublic(keySpec);
                    // Store Public Key
                    FXScreen.USER.getKeyStore().storePublicKey(pubKey, alias);
                    if (publish.getTopic().toString().contains(MyTopicSubscriptions.myPublisher1)) {
                        FXScreen.USER.getKeyStore().hasPublisherOneKeyInKeyStore = true;
                    }
                    if (publish.getTopic().toString().contains(MyTopicSubscriptions.myPublisher2)) {
                        FXScreen.USER.getKeyStore().hasPublisherTwoKeyInKeyStore = true;
                    }

                }

                // Check if publisher was Publisher 1
                if (publish.getTopic().toString().contains(MyTopicSubscriptions.myPublisher1 + "/rpi")) {
                    // Check if we have a Publishers'1 key
                    if (checkKeyIsInKeyStoreForError(FXScreen.USER.getKeyStore().hasPublisherOneKeyInKeyStore, publish.getTopic().toString())) {
                        // Update myPublisher1 tiles
                        PublisherTilesController.setPublisherDataInTiles(publish, MyTopicSubscriptions.myPublisher1,
                                pTwoMotionTile, pTwoImageTile, pTwoBuzzerTile, pTwoHumidTile, pTwoTempTile);
                    }
                }
                // Check if publisher was Publisher 2
                if (publish.getTopic().toString().contains(MyTopicSubscriptions.myPublisher2 + "/rpi")) {
                    // Check if we have a Publisher two's key
                    if (checkKeyIsInKeyStoreForError(FXScreen.USER.getKeyStore().hasPublisherTwoKeyInKeyStore, publish.getTopic().toString())) {
                        // Update myPublisher2 tiles
                        PublisherTilesController.setPublisherDataInTiles(publish, MyTopicSubscriptions.myPublisher2,
                                pOneMotionTile, pOneImageTile, pOneBuzzerTile, pOneHumidTile, pOneTempTile);
                    }
                }
            } catch (UnsupportedEncodingException | NoSuchAlgorithmException | SignatureException
                    | NoSuchProviderException | InvalidKeyException ex) {
                logger.severe("Could not verify Signature. Please Check with Admin.");
            } catch (InvalidAlgorithmParameterException | NoSuchPaddingException | IllegalBlockSizeException
                    | InvalidKeySpecException | BadPaddingException | KeyStoreException
                    | UnrecoverableKeyException ignored) {
                logger.severe("Contact Admin. - erorr in getting publisher");
            }
        });
        System.out.println("STARTED TO OBSERVE MY SUBSCRIBED TOPICS.");
    }

    /**
     * This method checks fi a key in the Users' keystore. If it is not it
     * displays an error messaage.
     *
     * @param isKeyIn : Boolean - known with KStore.isPublicKeyInKeyStore
     * @return boolean : true if key exists, false otherwise
     */
    private boolean checkKeyIsInKeyStoreForError(boolean isKeyIn, String publisher) {
        if (isKeyIn) {
            Platform.runLater(() -> FXScreen.myErrorDisplayTile.setDescription(""));
            return true;
        } else {
            Platform.runLater(() -> FXScreen.myErrorDisplayTile.setDescription("Public key of " + publisher + " is not recognized. Please Upload public Key."));
            return false;
        }
    }

    /**
     * Used for Submit Button This method sets/ an error message to the button
     * and makes it background to red. It also makes the User to Null.
     *
     * @param btn : Submit button for user to connect of HiveMQ
     * @param message : Error Message to Display
     */
    private void setButtonErrorStyle(Button btn, String message) {
        btn.setText(message);
        btn.setStyle("-fx-background-color: #FF0000");
        FXScreen.USER = null;
    }

    /**
     * This method creates an Image Tile
     * @param image - image path to in resources
     * @param title - Title of the Tile
     * @return TileFx Tile
     */
    private Tile ImageTileGenerator(String image, String title) {
        return TileBuilder.create()
                .skinType(SkinType.IMAGE)
                .prefSize(WIDTH, HEIGHT)
                .textSize(TextSize.SMALL)
                .image(new Image(this.getClass().getResourceAsStream(image)))
                .title(title)
                .translateX(12)
                .textColor(Color.BLUEVIOLET)
                .backgroundColor(Color.valueOf("#393d42"))
                .borderWidth(2)
                .borderColor(Color.BLUEVIOLET)
                .textColor(Color.BLUEVIOLET)
                .textAlignment(TextAlignment.CENTER)
                .build();
    }

    /**
     * This method create a Tile of the specified SkinType
     * @param type - Skin Type of Tile
     * @param title - Title of Tile
     * @param unit - Unit in tile, Empty for nothing
     * @return TileFx Tile
     */
    private Tile createTile(SkinType type, String title, String unit) {
        return TileBuilder.create()
                .skinType(type)
                .prefSize(WIDTH, HEIGHT)
                .textSize(TextSize.BIGGER)
                .translateX(15)
                .textColor(Color.BLUEVIOLET)
                .title(title)
                .unit(unit)
                .backgroundColor(Color.valueOf("#393d42"))
                .borderWidth(2)
                .borderColor(Color.BLUEVIOLET)
                .build();
    }
    
    /**
     * Start Analyzing Humidity and Temperature Sensor
     */
    private void startDisplaySensorsData() {
        Thread exampleThread = new Thread(() -> {
            while (running) {
                try {
                    // Delay 5 seconds
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    System.err.println("exampleThread thread got interrupted");
                }
                // Needed to be to update an active node
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        String TempOutput = "";
                        String HumidityOutput = "";
                        String TimeStamp = "";
                        try {
                            TempOutput = theProcessBuilderDht.startProcessDHT("Temperature");
                            HumidityOutput = theProcessBuilderDht.startProcessDHT("Humidity");
                            TimeStamp = theProcessBuilderDht.getTimeStamp();
                        } catch (IOException e) {
                        }
                        myTempTile.setValue(Double.parseDouble(TempOutput));
                        myHumidTile.setValue(Double.parseDouble(HumidityOutput));
                        // har_tempTile.setText("Temp detected at: " + TimeStamp);
                        myHumidTile.setDescription("Humidity and Temp detected at: " + TimeStamp);
                        logger.info("DETECTED HUMIDITY AND TEMPERATURE ");

                        if (isUserLoggedIn) {
                            String messageTemperature = "{\"message\":{\"timestamp\":\"" + TimeStamp + "\",\"temperature\":"
                                    + TempOutput + "}}";
                            String messageHumidity = "{\"message\":{\"timestamp\":\"" + TimeStamp + "\",\"humidity\":"
                                    + HumidityOutput + "}}";
                            publishMyRPIInformation(messageTemperature, MqttTopicsOfUsersRPI.RPI_TEMPERATURE);
                            publishMyRPIInformation(messageHumidity, MqttTopicsOfUsersRPI.RPI_HUMIDITY);
                            logger.info("HUMIDITY AND TEMPERATURE DATA SENT.");

                        }
                    }
                });
            }
        });

        // Start the thread
        exampleThread.start();
    }

    /**
     * Start Analyzing Buzzer Sensor
     */
    private void startDisplayBuzzerData() {
        Thread exampleThread = new Thread(() -> {
            while (running) {
                try {
                    // Delay 5 seconds
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    System.err.println("exampleThread thread got interrupted");
                }
                // Needed to be to update an active node
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        String TimeDoorbellOutput = "";
                        try {

                            TimeDoorbellOutput = theProcessBuilderDoorbell.startProcessDoorbell();
                        } catch (IOException e) {
                        }
                        try {
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                            LocalDateTime dateTime = LocalDateTime.parse(TimeDoorbellOutput, formatter);
                            myBuzzerTile.setDescription("Buzzer pressed at: " + dateTime.format(formatter));
                            logger.info("BUZZER WAS PRESSED");

                            if (isUserLoggedIn) {
                                String messageBuzzer = "{\"message\":{\"timestamp\":\"" + dateTime.format(formatter) + "\"}}";
                                publishMyRPIInformation(messageBuzzer, MqttTopicsOfUsersRPI.RPI_BUZZER);
                                logger.info("BUZZER DATA WAS SENT");

                            }
                        } catch (DateTimeParseException e) {
                            logger.info("User has not pressed buzzer ");
                        }

                    }
                });
            }
        });

        // Start the thread
        exampleThread.start();
    }
    /**
     * Start Analyzing Motion Sensor
     */
    private void startDisplayMotionData() {
        Thread exampleThread = new Thread(() -> {
            while (running) {
                try {
                    // Delay 5 seconds
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    System.err.println("exampleThread thread got interrupted");
                }
                // Needed to be to update an active node
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {

                        String TimeMotionOutput = "";
                        try {
                            TimeMotionOutput = theProcessBuilderSesnsor.startProcessSensor();
                        } catch (IOException e) {
                        }
                        //prase timeDoorbellOutput to LocalDateTime
                        BufferedImage image;
                        byte[] imageBytes = null;

                        try {
                            image = ImageProcessor.getLatestTakenPicture();
                            imageBytes = ImageProcessor.toByteArray(image, "png");
                            Image img = new Image(new ByteArrayInputStream(imageBytes));
                            myImageTile.setImage(img);
                        } catch (IOException ex) {
                            logger.warning("Image could not be gotten from path defined in ImageProcessor.");
                        }
                        try {
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                            LocalDateTime dateTime = LocalDateTime.parse(TimeMotionOutput, formatter);
                            myMotionTile.setDescription("Motion detected at: " + dateTime.format(formatter));
                            logger.info("MOTION DETECTED");
                            // Send Data
                            if (isUserLoggedIn) {
                                String imageBase64 = HashingCode.base64EncodeString(imageBytes);
                                String messageMotion = "{\"message\":{\"timestamp\":\"" + dateTime.format(formatter) + "\",\"image\":\"" + imageBase64 + "\"}}";
                                publishMyRPIInformation(messageMotion, MqttTopicsOfUsersRPI.RPI_MOTION);
                                logger.info("MOTION DATA WAS SENT");
                            }
                        } catch (DateTimeParseException e) {
                            logger.info("NO MOTION DETECTED");

                        }

                    }

                });
            }
        });

        // Start the thread
        exampleThread.start();
    }

    /**
     * This method published the message to HiveMQ. It creates a signature and
     * encodes it, it also encodes the message to be sent to keep data safety
     * and integrity.
     *
     * @param message : Message to send
     * @param topicOfData : Topic endpoint to send to
     */
    private void publishMyRPIInformation(String message, String topicOfData) {
        try {
            byte[] signature = KStore.generateSignature(FXScreen.USER.getKeyStore().getPrivateKey(), message);
            String signatureEncodedBase64 = HashingCode.base64EncodeString(signature);
            String messageEncodedBase64 = HashingCode.base64EncodeString(message.getBytes(UTF_8));
            String toSend = "{\"signature\":\"" + signatureEncodedBase64
                    + "\",\"message\": \"" + messageEncodedBase64
                    + "\",\"alias\":\"" + MyTopicSubscriptions.whoAmI + "-pk"
                    + "\"}";
            FXScreen.USER.getMyMqttConnection().publishWith()
                    .topic(MyTopicSubscriptions.whoAmI + "/rpi/" + topicOfData)
                    .payload(toSend.getBytes(UTF_8))
                    .send();
            logger.info("DATA WAS PUBLISHED");
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException | SignatureException e) {
            logger.log(Level.SEVERE, "Could not create a signature");
        }
    }

    /**
     * Method to call when exiting application
     */
    private void endApplication() {
        FXScreen.running = false;
        FXScreen.USER.getMyMqttConnection().disconnect();
        Platform.exit();
    }
}
