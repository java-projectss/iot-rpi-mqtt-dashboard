package com.mycompany.utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * This class is to hold utility methods for images.
 */
public class ImageProcessor {
    private static final String myPicturePath = "/home/davit/Pictures";
    /**
     * This method converts a BufferedImage to a byte array.
     *
     * @param bi     BufferedImage created from picture
     * @param format Format of the image i.e. png,jpg
     * @return byte[]
     * @throws IOException : Exception when writing
     */
    public static byte[] toByteArray(BufferedImage bi, String format)
            throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi, format, baos);
        return baos.toByteArray();

    }

    /**
     * This method gets the latest taken picture from the myPicturePath variable
     * @return BufferedImage : latest picture taken
     * @throws IOException : Exception when creating Image
     */
    public static BufferedImage getLatestTakenPicture() throws IOException {
        //Creating a File object for directory
        File directoryPath = new File(myPicturePath);
        //List of all files and directories
        File[] files = directoryPath.listFiles();
        System.out.println("List of files and directories in the specified directory:");
        System.out.println(directoryPath.getAbsolutePath().toString());

        assert files != null;
        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return ImageIO.read(lastModifiedFile);
    }


    /**
     * This method converts a byte array to a Buffered Image.
     *
     * @param bytes : bytes array to convert
     * @return BufferedImage
     * @throws IOException : Exeption when creating BufferedImage
     */
    public static BufferedImage toBufferedImage(byte[] bytes) throws IOException {
        return ImageIO.read(new ByteArrayInputStream(bytes));

    }
}
