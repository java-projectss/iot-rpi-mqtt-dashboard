package com.mycompany.crypto;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import java.security.*;
import java.util.Base64;

/**
 * @author Carlton Davis
 * Example code illustrating Symmetric key generation, encryption/decryption of
 * strings and files using AES in GCM mode.
 */
public class HashingCode {
    public static final int GCM_IV_LENGTH = 12;
    public static final int GCM_TAG_LENGTH = 16;

//    public static final String ALGORITHM_HASH = "AES/GCM/NoPadding";

    /**
     * This method encodes a message of byte[]
     * @param message : byte[] of the string you want to encode
     * @return String : Base64 encoded String
     */
    public static String base64EncodeString(byte[] message){
        return Base64.getEncoder().encodeToString(message);
    }

    /**
     * This method decodes a Base64 encoded string
     * @param message : Encoded message
     * @return byte[] : decoded message
     */
    public static byte[] base64DecodeString(String message){
        return Base64.getDecoder().decode(message);
    }
    /**
     * This method generates the hash of a password
     *
     * @return String : Hashed Password
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidKeyException
     */
    public static String generateHashedPassword(String password, SecretKey keyFromKeyStore) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        byte[] byteHashingArray = HashingCode.generateGCMIV();
        return HashingCode.encryptStr(password, keyFromKeyStore, byteHashingArray);
    }


    /**
     * Method for generating 12 byte GCM Initialization Vector.
     *
     * @return
     */
    public static byte[] generateGCMIV() {
        byte[] GCMIV = new byte[GCM_IV_LENGTH];
        SecureRandom random = new SecureRandom();
        random.nextBytes(GCMIV);
        return GCMIV;
    }

    /**
     * *
     * Method to encrypt strings: takes an algorithm, string data, a secret key
     * and IV.
     */
    public static String encryptStr(String data, SecretKey key, byte[] IV)
            throws NoSuchAlgorithmException, BadPaddingException,
            NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException {
        String algorithm = "AES/GCM/NoPadding";
        //Create an instance of the Cipher class
        Cipher cipher = Cipher.getInstance(algorithm);

        // Create GCMParameterSpec
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV);

        // Initialize Cipher for ENCRYPT_MODE
        cipher.init(Cipher.ENCRYPT_MODE, key, gcmParameterSpec);

        //Perform encryption
        byte[] cipherText = cipher.doFinal(data.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    /**
     * Method for decrypting an input string
     *
     * @param cipherText
     * @param key
     * @param IV
     * @return
     * @throws NoSuchAlgorithmException
     * @throws BadPaddingException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     */
    public static String decryptStr(String cipherText,
                                    SecretKey key, byte[] IV)
            throws NoSuchAlgorithmException, BadPaddingException,
            NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException {
        String algorithm = "AES/GCM/NoPadding";
        //Create an instance of the Cipher class.
        Cipher cipher = Cipher.getInstance(algorithm);

        // Create GCMParameterSpec
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV);

        // Initialize Cipher for ENCRYPT_MODE
        cipher.init(Cipher.DECRYPT_MODE, key, gcmParameterSpec);

        //Perform decryption
        byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText));

        return new String(plainText);
    }
}

