package com.mycompany.keyStore;

import com.mycompany.FXScreen;
import com.mycompany.crypto.HashingCode;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

/**
 * This class loads a key stores.
 * @author Davit Voskerchyan
 */
public class KStore {
    private final String curveName = "secp256r1";
    public static final String SIGNATURE_ALOGIRTHM = "SHA256withECDSA";
    private final String KEY_STORE_PATH;
    private KeyStore ks;
    private final KeyPair keyPair;
    private Certificate certificate;
    public static final String SECRET_KEY_ALIAS = "secretKey";
    private SecretKey secretKey;
    public boolean hasPublisherOneKeyInKeyStore;
    public boolean hasPublisherTwoKeyInKeyStore;
    private final char[] password;

    public KStore(String keyStorePath, String alias, char[] password) throws IOException {
        try {
            hasPublisherTwoKeyInKeyStore = false;
            hasPublisherOneKeyInKeyStore = false;
            this.KEY_STORE_PATH = keyStorePath;
            this.password = password;
            this.keyPair = this.getKeyPair(alias, password);
            generateSymmetricKey(password);
            System.out.println("Key Store was Created.");
        } catch (UnrecoverableEntryException | NoSuchAlgorithmException | KeyStoreException | CertificateException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This method is to get the Key Stores'  private key
     *
     * @return PrivateKey
     */
    public PrivateKey getPrivateKey() {
        return this.keyPair.getPrivate();
    }

    /**
     * This method is to get the Key Stores'  public key
     *
     * @return PublicKey
     */
    public PublicKey getPublicKey() {
        return this.keyPair.getPublic();
    }

    /**
     * This method is to get the Key Stores' certificate.
     *
     * @return Certificate
     */
    public Certificate getCertificate() {
        return this.certificate;
    }

    /**
     * This method is to retrieve key
     *
     * @param alias    : String - alias of the Key when it was stored
     * @param password : char[] - password to be able to retrieve the key
     * @return Key
     */
    public Key getKeyFromKeyStore(String alias, char[] password) throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException {
        return this.ks.getKey(alias, password);
    }

    /**
     * This method saves the Key Store with all the manipulation done to it.
     * @param name : String - Name of the new Key Store | overrides old key store if same name
     * @param password : char[] - Password to store the keystore with.
     */
    public void storeAwayKeyStore(String name, char[] password) throws CertificateException, KeyStoreException, NoSuchAlgorithmException, IOException {
        try (FileOutputStream fos = new FileOutputStream(name)) {
            ks.store(fos, password);
            System.out.println("Key Store was stored/saved.");
        }
    }

    /**
     * This method store the public key as a secret Key.
     * Its a way around of storing just a public key since it is not aloowed
     * @param pk : Public key to be store
     * @param alias : Alias of the public Key
     */
    public void storePublicKey(PublicKey pk, String alias) throws InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, KeyStoreException {
        SecretKey sk = new SecretKeySpec(pk.getEncoded(), pk.getAlgorithm());
        KeyStore.SecretKeyEntry skEntry = new KeyStore.SecretKeyEntry(sk);
        KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(this.password);
        ks.setEntry(alias, skEntry, protParam);
    }

    /**
     * This method gets a secret key which is a public key shared from MQTT by publishers
     * @param alias : alias of the key
     * @return SecretKey : A secretKey which acts like a public key
     */
    public SecretKey getSharedPublicKeyFromMQTT(String alias) throws InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, UnrecoverableKeyException, KeyStoreException {
        return ((SecretKey)this.ks.getKey(alias, this.password));
    }


    /**
     * This method generates a Secret Key for the KeyStore.
     */
    private void generateSymmetricKey(char[] password) throws NoSuchAlgorithmException, KeyStoreException {
        SecretKey mySecretKey = this.GenerateSecretKey(256);
        // Password Protection
        KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(password);
        KeyStore.SecretKeyEntry skEntry = new KeyStore.SecretKeyEntry(mySecretKey);
        this.secretKey = skEntry.getSecretKey();
        ks.setEntry(KStore.SECRET_KEY_ALIAS, skEntry, protParam);
    }

    /**
     * Method for generating secret key takes an integer n; n can be 128, 192 or
     * 256.
     * Copied from Carlton
     */
    private SecretKey GenerateSecretKey(int n) throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(n); //Initialize the key generator
        return keyGenerator.generateKey();
    }


    /**
     * This method creates a KeyPair associated with the key stores Private and Public Key.
     * As well as assigns the Certificate.
     *
     * @return KeyPair : a pair of public and private key
     */
    private KeyPair getKeyPair(String alias, char[] password) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {
        FileInputStream is = new FileInputStream(this.KEY_STORE_PATH);

        this.ks = KeyStore.getInstance("PKCS12");
        this.ks.load(is, password);

        is.close();

        Key key = this.ks.getKey(alias, password);
        // Get certificate of public key
        this.certificate = this.ks.getCertificate(alias);
        // Get public key
        PublicKey publicKey = this.certificate.getPublicKey();
        // Return a key pair
        return new KeyPair(publicKey, (PrivateKey) key);
    }

    /**
     * Method for verifying digital signature.
     * Copied from Carlton
     */
    public static boolean verifySignature(byte[] signature, PublicKey publickey, String message)
            throws NoSuchAlgorithmException, NoSuchProviderException,
            InvalidKeyException, UnsupportedEncodingException, SignatureException {

        //Create an instance of the signature scheme for the given signature algorithm
        Signature sig = Signature.getInstance(SIGNATURE_ALOGIRTHM, "SunEC");

        //Initialize the signature verification scheme.
        sig.initVerify(publickey);

        //Compute the signature.
        sig.update(message.getBytes(StandardCharsets.UTF_8));

        //Verify the signature.
        boolean validSignature = sig.verify(signature);

        if (validSignature) {
            System.out.println("\nSignature is valid");
        } else {
            System.out.println("\nSignature is NOT valid!!!");
        }

        return validSignature;
    }

    /**
     * Method for generating digital signature.
     * Copied from Carlton
     */
    public static byte[] generateSignature(PrivateKey privatekey, String message)
            throws NoSuchAlgorithmException, NoSuchProviderException,
            InvalidKeyException, SignatureException {

        //Create an instance of the signature scheme for the given signature algorithm
        Signature sig = Signature.getInstance(SIGNATURE_ALOGIRTHM, "SunEC");

        //Initialize the signature scheme
        sig.initSign(privatekey);

        //Compute the signature
        sig.update(message.getBytes(StandardCharsets.UTF_8));

        return sig.sign();
    }


}
