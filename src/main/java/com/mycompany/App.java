package com.mycompany;

import java.io.IOException;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    //Allows the stage be easily accessible
    public static Stage theStage;

    @Override
    public void start(Stage stage) throws IOException {
        var scene = new Scene(new FXScreen(), 1200, 610);
        stage.setScene(scene);
        
        stage.setTitle("HiveMQ DashBoard");
        stage.show();

        // Show Stage
        App.theStage = stage; // I'm not sure for this implementation
        stage.show();

        // Make sure the application quits completely on close
        theStage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });
    }
}