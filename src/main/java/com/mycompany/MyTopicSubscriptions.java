package com.mycompany;

import com.hivemq.client.mqtt.mqtt5.Mqtt5BlockingClient;
import com.mycompany.mqtt.MqttTopicsOfUsersRPI;

/**
 * This class is for the user to choose to whom he will subscribe.
 */
public class MyTopicSubscriptions {
    /**
     * TODO: CHANGE THE publishers assignment to the publishers you want to subscribe.
     * HENCE, CHANGE THE TEXT AFTER myPublisher1 and myPublisher2 :
     * i.e.  myPublisher1 = MqttTopicsOfUsersRPI.FirstNameLastNameTopic
     * i.e.  myPublisher2 = MqttTopicsOfUsersRPI.FirstNameLastNameTopic
     */
    public static final String whoAmI = MqttTopicsOfUsersRPI.CuneytYildirimTopic;
    public static final String myPublisher1 = MqttTopicsOfUsersRPI.DavitVoskerchyanTopic;
    public static final String myPublisher2 = MqttTopicsOfUsersRPI.HaroutDabbaghianTopic;
    
    /**
     * This method is where the User can modify to whom topics they want to subscribe.
     *
     * @param client : Users' connection to the HiveMQ Cluster
     */
    public static void subscribeToMyTopics(Mqtt5BlockingClient client){
        /*
         * TODO: CHANGE THE VALUES UNDERNEATH TO SUBSCRIBE TO DIFFERENT USERS.
         * CHANGE AFTER MqttTopicsOfUsersRPI.subscribeTo<change this part>Topics(client)
         * YOU CAN CHANGE IT Any Names in the topics that is : firstname-lastname/pi/+
         * + stands for anything
         * THERE SHOULD BE A MAXIMUM OF TWO SUBSCRIPTIONS.
         */
       MqttTopicsOfUsersRPI.subscribeToDavitVoskerchyanTopics(client);
//        MqttTopicsOfUsersRPI.subscribeToCuneytYildirimTopics(client);
         MqttTopicsOfUsersRPI.subscribeToHaroutDabbaghianTopics(client);
    }
}
