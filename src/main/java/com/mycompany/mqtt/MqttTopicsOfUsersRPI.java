package com.mycompany.mqtt;

import com.hivemq.client.mqtt.mqtt5.Mqtt5BlockingClient;

/**
 * This class holds the subscription of different users to their Raspberry Pies.
 * The topic is firstname-lastname/pi/+
 * @author Davit Voskerchyan
 */
public class MqttTopicsOfUsersRPI {
    public final static String RPI_MOTION = "motion";
    public final static String RPI_BUZZER = "buzzer";
    public final static String RPI_TEMPERATURE = "temperature";
    public final static String RPI_HUMIDITY = "humidity";

    /**
     * This method Subscribes to Davit Voskerchyan users topics
     * @param myClient : Connection of the Users MQTT
     */
    public final static String DavitVoskerchyanTopic = "davit-voskerchyan";
    public static void subscribeToDavitVoskerchyanTopics(Mqtt5BlockingClient myClient){
        MqttConnection.subscribeToTopic(myClient, DavitVoskerchyanTopic+"/publicKey");
        MqttConnection.subscribeToTopic(myClient, DavitVoskerchyanTopic+"/rpi/+");
    }

    /**
     * This method Subscribes to Davit Voskerchyan users topics
     * @param myClient : Connection of the Users MQTT
     */
    public final static String HaroutDabbaghianTopic = "harout-dabbaghian";
    public static void subscribeToHaroutDabbaghianTopics(Mqtt5BlockingClient myClient){
        MqttConnection.subscribeToTopic(myClient, HaroutDabbaghianTopic+"/publicKey");
        MqttConnection.subscribeToTopic(myClient, HaroutDabbaghianTopic+"/rpi/+");
    }

    /**
     * This method Subscribes to Cuneyt Yildirim users topics
     * @param myClient : Connection of the Users who wishes to subscribe.
     */
    public final static String CuneytYildirimTopic = "cuneyt-yildirim";
    public static void subscribeToCuneytYildirimTopics(Mqtt5BlockingClient myClient){
        MqttConnection.subscribeToTopic(myClient, CuneytYildirimTopic+"/publicKey");
        MqttConnection.subscribeToTopic(myClient, CuneytYildirimTopic+"/rpi/+");
    }

}

