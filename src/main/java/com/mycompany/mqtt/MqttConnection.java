package com.mycompany.mqtt;

import com.hivemq.client.mqtt.MqttClient;
import com.hivemq.client.mqtt.exceptions.ConnectionFailedException;
import com.hivemq.client.mqtt.mqtt5.Mqtt5BlockingClient;
import com.hivemq.client.mqtt.mqtt5.exceptions.Mqtt5ConnAckException;
import com.hivemq.client.mqtt.mqtt5.message.connect.connack.Mqtt5ConnAck;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * This class hold the methods to communicate with the Hive MQ cloud.
 * @author Davit Voskerchyan
 */
public class MqttConnection{
    public static final int PORT = 8883;
    public static final String HOST = "d37a661458074f69969b65c869536385.s1.eu.hivemq.cloud";

    /**
     * This method creates a connection to the cluster
     *
     * @return Mqtt5BlockingClient : connection to clusetr
     */
    public static Mqtt5BlockingClient createConnectionToMqttClient() throws IllegalArgumentException {
        // Create an MQTT client
        return MqttClient.builder()
                .useMqttVersion5()
                .serverHost(HOST)
                .serverPort(PORT)
                .sslWithDefaultConfig()
                .buildBlocking();
    }


    /**
     * This method creates a connection to the hive mq cloud cluster.
     *
     * @param myClient : Blocking Client connection to cluster.
     * @param username : username of the user in the cluster
     * @param password : password of the user in the cluster
     * @return Mqtt5ConnAck : connection to hive Mq Client
     * @throws Mqtt5ConnAckException : Exception if login information is incorrect
     */
    public static void createConnectionToMyMqttUser(Mqtt5BlockingClient myClient, String username, String password) throws Mqtt5ConnAckException, ConnectionFailedException {
        // connect to HiveMQ Cloud with TLS and username/pw
        myClient.connectWith()
                .simpleAuth()
                .username(username)
                .password(UTF_8.encode(password))
                .applySimpleAuth()
                .send();
    }


    /**
     * This method subscribes to a topic published in the cloud.
     * @param myClient : Connection of the user to the cluster.
     * @param topic : Topic to subscribe to.
     */
    public static void subscribeToTopic(Mqtt5BlockingClient myClient, String topic){
        myClient.subscribeWith()
                .topicFilter(topic)
                .send();
    }
}
