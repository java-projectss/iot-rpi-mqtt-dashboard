package com.mycompany.publisherTiles;

import com.hivemq.client.mqtt.mqtt5.message.publish.Mqtt5Publish;
import com.mycompany.FXScreen;
import com.mycompany.crypto.HashingCode;
import com.mycompany.keyStore.KStore;
import com.mycompany.mqtt.MqttTopicsOfUsersRPI;
import eu.hansolo.tilesfx.Tile;

import java.io.ByteArrayInputStream;

import javafx.application.Platform;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

import javafx.scene.image.Image;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 * This class is a controller class. It sets the data received from a publisher
 * to the corresponding tiles.
 *
 * @author Davit Voskerchyan
 */
public class PublisherTilesController {

    /**
     * This method sets a Tile values: This is for tiles like PercentageTile or
     * GaugeTile
     *
     * @param tile : Tile to change value
     * @param title : Title of tile
     * @param number : Double value to set
     */
    private static void setTileValue(Tile tile, String title, Double number) {
        Platform.runLater(() -> {
            tile.setTitle(title);
            tile.setValue(number);
        });

    }

    /**
     * This method sets a Tiles text: This tile is for tiles like TextFieldTile,
     * SwitchTile
     *
     * @param tile : Tile to change value
     * @param title : Title of tile
     * @param text : Text to set for tile
     */
    private static void setTileText(Tile tile, String title, String text) {
        Platform.runLater(() -> {
            tile.setTitle(title);
            tile.setDescription(text);
        });
    }

    /**
     * This method sets the data received for the first publisher.
     *
     * @param publish : Published Data by publisher EXAMPLE OF DATA: {
     * "message": { "timestamp" : "2022-12-11-10:10:59", "image": "bytes",
     * "temperature": 12, "humidity": 12 }, "signature": [1,2,3,4,5,6,7,8,9,0] }
     */
    public static void setPublisherDataInTiles(Mqtt5Publish publish, String publisher, Tile motionTile, Tile imageTile, Tile buzzerTile,
            Tile humidityTile, Tile temperatureTile) throws UnsupportedEncodingException, NoSuchAlgorithmException, SignatureException, NoSuchProviderException, InvalidKeyException, InvalidAlgorithmParameterException, UnrecoverableKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, KeyStoreException, InvalidKeySpecException {
        System.out.println("tTRYING TO SET PUBLISHER DATa");
        String topic = publish.getTopic().toString();
        var dataPayload = new JSONObject(UTF_8.decode(publish.getPayload().get()).toString());
        var publisherName = publisher + " ";
        // Get Encoded Signature and Message
        byte[] signature = Base64.getDecoder().decode(dataPayload.getString("signature").getBytes(UTF_8));
        // Decoding Encoded Message with the data
        // TODO IF BREAKS HERE CHECK HERE FOR BASE DECODING
        String messageDecoded = new String(HashingCode.base64DecodeString(dataPayload.getString("message")));
        // Get Publishers Public Key
        SecretKey s = FXScreen.USER.getKeyStore().getSharedPublicKeyFromMQTT(dataPayload.getString("alias"));
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(s.getEncoded());
        KeyFactory keyFactory = KeyFactory.getInstance(FXScreen.USER.getKeyStore().getPublicKey().getAlgorithm());
        PublicKey pubKey = keyFactory.generatePublic(keySpec);
        // Verify Signature of message
        boolean isSignatureVerified = KStore.verifySignature(signature, pubKey, messageDecoded);
        if (!isSignatureVerified) {
            Platform.runLater(() -> {
                FXScreen.myErrorDisplayTile.setDescription("Wrong Signature - message won't be displayed.");
            });
            return;
        }
        System.out.println("SIGNARTURE VERFIED");

        // Create Json Object from message
        var data = new JSONObject(messageDecoded);
        // Display Data
        displayDataAccordingToTopic(motionTile, imageTile, buzzerTile, humidityTile, temperatureTile, topic, publisherName, data);
    }

    /**
     * This method matches the topic to its corresponding tile and displays the
     * data recevied
     *
     * @param motionTile,imageTile,buzzerTile,humidityTile,temperatureTile :
     * Tile to display data in
     * @param topic : Topic of the published message
     * @param publisherName : Name of the publisher
     * @param data : Payload that was sent
     */
    private static void displayDataAccordingToTopic(Tile motionTile, Tile imageTile, Tile buzzerTile,
            Tile humidityTile, Tile temperatureTile, String topic, String publisherName, JSONObject data) {
        // Check for motion topic
        if (topic.contains(MqttTopicsOfUsersRPI.RPI_MOTION)) {

            // Set Image
            byte[] imageDecoded = Base64.getDecoder().decode(data.getJSONObject("message").getString("image").getBytes(UTF_8));
            Image img = new Image(new ByteArrayInputStream(imageDecoded));
            Platform.runLater(() -> {
                setTileText(motionTile, publisherName + MqttTopicsOfUsersRPI.RPI_MOTION,
                        "Motion detected at " + data.getJSONObject("message").getString("timestamp"));
                imageTile.setImage(img);
            });
            System.out.printf("DISPLAYING RPI_MOTION AND IMAGE %s \n", publisherName);
        }

        // Check for buzzer topic
        if (topic.contains(MqttTopicsOfUsersRPI.RPI_BUZZER)) {
            Platform.runLater(() -> {

                setTileText(buzzerTile, publisherName + MqttTopicsOfUsersRPI.RPI_BUZZER,
                        "Buzzer was pressed at " + data.getJSONObject("message").getString("timestamp"));
            });
            System.out.printf("DISPLAYING RPI_BUZZER %s \n", publisherName);
        }
        // Check for humidity topic
        if (topic.contains(MqttTopicsOfUsersRPI.RPI_HUMIDITY)) {
            Platform.runLater(() -> {
                setTileValue(humidityTile, publisherName + MqttTopicsOfUsersRPI.RPI_HUMIDITY,
                        data.getJSONObject("message").getDouble("humidity"));
                humidityTile.setDescription("REGISTERED AT - " + data.getJSONObject("message").getString("timestamp"));

            });

            System.out.printf("DISPLAYING RPI_HUMIDITY for %s \n", publisherName);
        }
        // Check for temperature topic
        if (topic.contains(MqttTopicsOfUsersRPI.RPI_TEMPERATURE)) {
            Platform.runLater(() -> {
                setTileValue(temperatureTile, publisherName + MqttTopicsOfUsersRPI.RPI_TEMPERATURE,
                        data.getJSONObject("message").getDouble("temperature"));
                temperatureTile.setDescription("REGISTERED AT - " + data.getJSONObject("message").getString("timestamp"));
            });

            System.out.printf("DISPLAYING RPI_TEMPERATURE %s \n", publisherName);
        }
    }
}
