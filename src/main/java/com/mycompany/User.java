package com.mycompany;

import com.mycompany.crypto.HashingCode;
import com.hivemq.client.mqtt.exceptions.ConnectionFailedException;
import com.hivemq.client.mqtt.mqtt5.Mqtt5BlockingClient;
import com.hivemq.client.mqtt.mqtt5.exceptions.Mqtt5ConnAckException;
import com.mycompany.keyStore.KStore;
import com.mycompany.variables.Variables;
import com.mycompany.mqtt.MqttConnection;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.io.IOException;
import java.security.*;

/**
 * This class describes a User.
 */
public class User {
    private final String username;
    private final String hashingPass;
    private final Mqtt5BlockingClient myMqttClient;

    private final KStore keyStore;

    /**
     * @param username : username of the user for to connect to HiveMQ
     * @param password : password of the user for connect to HiveMQ
     * @throws Mqtt5ConnAckException : Exception if login information is incorrect.
     * @throws ConnectionFailedException : Exception if connecting to host has failed.
     */
    public User(String username, String password) throws IllegalArgumentException, ConnectionFailedException, Mqtt5ConnAckException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, IOException {
        this.username = username;
        this.keyStore = new KStore(Variables.ABS_PATH_TO_KEY_STORE, Variables.KEY_STORE_ALIAS, password.toCharArray());
        try{
            this.hashingPass = HashingCode.generateHashedPassword(password,
                    (SecretKey) this.keyStore.getKeyFromKeyStore(KStore.SECRET_KEY_ALIAS, password.toCharArray()) );
        }catch (KeyStoreException | UnrecoverableKeyException e){ throw new RuntimeException("Could not create User - check hashing of password in User Constructor. Contact Admin.");}
        this.myMqttClient = MqttConnection.createConnectionToMqttClient();
        MqttConnection.createConnectionToMyMqttUser(this.myMqttClient, username, password);
        MyTopicSubscriptions.subscribeToMyTopics(myMqttClient);
        System.out.println("Created MQTT Connection, Hashed Password, Key Store connection.");
    }



    /**
     * This method returns the User's username
     *
     * @return String : username
     */
    public String getUsername() {
        return username;
    }

    /**
     * This method returns the User's hashed password
     *
     * @return : hashed password
     */
    public String getHashingPass() {
        return hashingPass;
    }

    /**
     * This method returns the User connection to the cluster.
     *
     * @return
     */
    public Mqtt5BlockingClient getMyMqttConnection() {
        return myMqttClient;
    }

    /**
     * This method returns the Users' key store.
     * @return KStore : Key Store of the user
     */
    public KStore getKeyStore(){
        return this.keyStore;
    }

}
