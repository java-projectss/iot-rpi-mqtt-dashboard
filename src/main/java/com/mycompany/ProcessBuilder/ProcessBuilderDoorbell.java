package com.mycompany.ProcessBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Harout Dabbaghian
 */

public class ProcessBuilderDoorbell extends ProcessBuilderInital {
    private String theOutput;

    
     public ProcessBuilderDoorbell(String theApp) {
        super(theApp);
    }
 

     //Start the process and get the output
     public String startProcessDoorbell() throws IOException {
 
         //Initialize theOutput to null String
         this.theOutput = "";
 
         //Start the process
         var process = this.processBuilder.start();
 
         try (var reader = new BufferedReader(
                 new InputStreamReader(process.getInputStream()))) {
 
             String line;
 
             while ((line = reader.readLine()) != null) {
                 this.theOutput = this.theOutput + line;
             }
 
         }
         return this.theOutput;
     }
 
 
}

