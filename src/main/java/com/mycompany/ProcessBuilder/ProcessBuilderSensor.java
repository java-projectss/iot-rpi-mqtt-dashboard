
package com.mycompany.ProcessBuilder;

import com.mycompany.Camera.CameraApp;
import com.pi4j.Pi4J;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 *
 * @author Harout Dabbaghian
 */

public class ProcessBuilderSensor extends ProcessBuilderInital {
    
     public ProcessBuilderSensor(String theApp) {
        super(theApp);
    }

    //Stores the output from the process
     private String theOutput;
 
     //Start the process and get the output
     public String startProcessSensor() throws IOException {
         
//         start();
         //Initialize theOutput to null String
         this.theOutput = "";
 
         //Start the process
         var process = this.processBuilder.start();
 
         try (var reader = new BufferedReader(
                 new InputStreamReader(process.getInputStream()))) {
 
             String line;
 
             while ((line = reader.readLine()) != null) {
                 this.theOutput = this.theOutput + line;
             }
 
         }
         if(!"off".equals(this.theOutput)){
             start();
         }
         
         return this.theOutput;
     }
     
       public void start() {

        //Initialize the Pi4J Runtime Context
        var pi4j = Pi4J.newAutoContext();

        CameraApp runApp = new CameraApp();
        runApp.execute(pi4j);
       
        // Shutdown Pi4J
        pi4j.shutdown();

    }
 
 
}
