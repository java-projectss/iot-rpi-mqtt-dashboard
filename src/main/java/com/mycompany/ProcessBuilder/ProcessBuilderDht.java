package com.mycompany.ProcessBuilder;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;

/**
 *
 * @author Harout Dabbaghian
 */

public class ProcessBuilderDht extends ProcessBuilderInital {

    //Stores the output from the process
    private String DhtValue;


    public ProcessBuilderDht(String theApp) {
        super(theApp);
    }


    public String startProcessDHT(String type) throws IOException {
        //type is either Temp or Humidity and returns the value
        this.DhtValue = "";

        //Start the process
        var process = this.processBuilder.start();

        try ( var reader = new BufferedReader(
                new InputStreamReader(process.getInputStream()))) {

            String line;

            if ((line = reader.readLine()) != null) {

                String[] theSplit = line.split(":");
                if (type.equals("Humidity")) {
                    this.DhtValue = theSplit[1];
                } else {
                    this.DhtValue = theSplit[3];
                }

            }
        }
        return this.DhtValue;
    }



    public String getTimeStamp() throws IOException {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return formatter.format(new Date());
    }

}

