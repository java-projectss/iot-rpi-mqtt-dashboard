package com.mycompany.ProcessBuilder;
import java.util.List;


/**
 *
 * @author Harout Dabbaghian
 */

public class ProcessBuilderInital {

    protected ProcessBuilder processBuilder;
 
     //The constructor to execute Python command takes a String
     public ProcessBuilderInital(String theApp) {
         this.processBuilder = new ProcessBuilder();
 
         //Determine if the OS is MS Windows
         boolean isWindows = System.getProperty("os.name")
                 .toLowerCase().startsWith("windows");
 
         //List to store the command and the command arguments
         List<String> commandAndArgs;
 
         //Setup the command based on the OS type
         if (isWindows) {
             commandAndArgs = List.of("C:\\Dev\\python3", theApp);
             this.processBuilder.command(commandAndArgs);
         }
         else {
             commandAndArgs = List.of("/usr/bin/python3", theApp);
             this.processBuilder.command(commandAndArgs);
         }
     }
    
}
