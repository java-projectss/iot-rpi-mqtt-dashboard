# HIVEMQ DASHBOARD

## Project
This project was created with 3 teammates. We used HiveMQ as our broker to send data of our RPI
with MQTT protocol.<br> We made a dashboard where we can see the data send from our teammates RPI.<br>
We send the temperature, humidity, motion detection time, buzzer pressed time and Image taken during motion detection.

# Description:

- Our team created an application that uses the MQTT Client application to send different pieces of data to the users on our network.
  To test the data transfer, we added a few electrical components, first a DHT11 sensor that sends temperature and humidity data,
  we also created a door bell that when detected by our motion sensor, takes a picture and rings if the user clicks the door bell button.

- We used javafx to visualize the data transfer so that the user can see when other users' data is sharing their public key.


## UML Diagram

![UML](/misc/DataCom_UML.drawio.png)


## Image of BreadBoard
![BreadBoard](/misc/breadboard.jpg)

## Features
1. DashBoard to see Publisher's data and Your data - Temperature, Humidity, Motion TimeStam, Buzzer TimeStamp and Image taken when motion detected.
2. Publishing and Subcriping to topics on HiveMQ Cluster usin MQTT protocol.
3. KeyStore where Publishers public key is stored to verify signature of message.
4. ProcessBuilders to get sensor data from RPI.
5. Validating and Normalizing data received or sent.
6. All features were implemented inluded but not excluded to the features mentioned above.

## Requirements
1. Having Git Bash Installed in your System.
2. Having Java and MVN Configured in your System.
3. A Raspberry Pi with a 32-bit OS.
4. A BreadBoard with the correct Circuit.
5. A Camera V2.1 for a RPi connected to the RPi.
6. A HiveMQ Cluster
7. Follow the **"Set Up"** instructions.

## Set Up
1. Open a Git Bash terminal and type : <b> git clone https://gitlab.com/Cuneyt_Yildirim/datacom_mqtt_project.git </b>
2. Open the project in your preferred IDE <b>( We recommend NetBeans.) </b>
3. Open a Git Bash terminal and type: **whoami** 
4. Copy the result.
5. Open the **ImageProcessor.java** file located in **src/main/java/com/mycompany/utils/**
6. Change the variable myPicturePath to represent the path of your pictures.
   - **(i.e Replace /home/davit/Pictures to /home/<the Result of step 4>/Pictures)**.
7. In the terminal type the following command: **cd ./src/main/java/com/mycompany/keyStore/; keytool -genkey -keyalg EC -alias CRD -keystore MyKeyStore.ks**
8. Follow the steps shown in the console to complete the creation of the KeyStore.
   - Note: The password must at least be 8 characters.
9. In the terminal type the following command: **cd ./src/main/java/com/mycompany/keyStore/; pwd; cd ../../../../../../**
10. Copy the Result.
11. Open the File Variables.java located in **./src/main/java/com/mycompany/variables**.
12. Inside the Quotation Marks of the variable **ABS_PATH_TO_KEY_STORE** paste the result from **Step 10**.
    - **Note: If in step 8 you have replaced CRD - you will need change the value in the KEY_STORE_ALIAS to the changed value.**
    - **Note: If in step 8 you have replaced MyKeyStore.ks - you will need change the value in ABS_PATH_TO_KEY_STORE to the changed value.**
13. Open the File CameraApp.java located in **./src/main/java/com/mycompany/Camera change the output paths from ("/home/harout123/Pictures) to the result of Step 4**
14. Open your HiveMQ cluster in the web browser - https://console.hivemq.cloud/
![UML](/misc/HiveMQCluster.png)
15. Open The File MqttConnection.java located in **./src/main/java/com/mycompany/mqtt**
16. Replace the PORT with the Port displayed in your cluster 
17. Replace the Host with the URL displayed in your cluster
18. Click Manage Cluster on the browser in **Step 14**
19. Click The Access Management Option 
20. Create a username and password for the User - **THE PASSWORD MUST BE THE SAME AS IN STEP 8**
21. Click on WEB CLIENT And connect to the client.
22. Step Up The following Topics for your user - **Replace joshuah-josh with the first and lastName of your user.**
    - joshuah-josh/publicKey
    - joshuah-josh/rpi/buzzer
    - joshuah-josh/rpi/temperature
    - joshuah-josh/rpi/humidity
    - joshuah-josh/rpi/motion
23. Open the IDE
24. Open The File MqttTopicsOfUsersRPI.java located in **./src/main/java/com/mycompany/mqtt**\
![TOPIC](/misc/userTopic.png)
25. Copy and Paste the code section in the class.
26. Replace All DavitVoskerchyanTopic with your users FirstNameLastNameTopic in the copied code.
27. Replace davit-voskerchyan with the first-lastname in step 22.
28. Open The File MyTopicSubscriptions.java located in **./src/main/java/com/mycompany/*_*_
![MYPUBSUB](/misc/mypubSub.png)
29. **Replace MqttTopicsOfUsersRPI.CuneytYildirimTopic with MqttTopicsOfUsersRPI.<FirstNameLastNameTopic> done in step 26.**
30. Repeat Steps 22-27 to set up two other users.
31. Replace myPublisher1 and myPublisher2 to the users RPI information you'd like to have.
32. In the same file - in subscribeToMyTopics(Mqtt5BlockingClient client) function replace the subscribeTopics. <br>
**Replace them with the publishers you just created in step 30-31**.
33. **NOTE IF YOU HAVE NOT BUILT YOUR BREADBOARD ACCORDING TO THE IMAGE ABOVE THAN YOU MUST CHANGE ALL THE PINS NUMBERS IN THE Python CODE <br>
Located in /src/main/Python - Doorbell.py for the buzzer, MotionSensor.py for the motion,DHT11.py for the DHT11 pin**
![img.png](/misc/subToTopics.png)
34. YOU ARE NOW READY TO RUN YOUR APP. 
35. IN YOUR IDE - FIND THE RUN BUTTON AND RUN the app.


